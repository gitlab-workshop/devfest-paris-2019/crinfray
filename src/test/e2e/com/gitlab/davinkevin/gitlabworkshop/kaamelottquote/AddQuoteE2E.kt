package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote

import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote.Quote
import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote.QuoteRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBodyList

@Tag("e2e")
@SpringBootTest
@AutoConfigureWebTestClient
class AddQuoteE2E {

    @Autowired
    lateinit var rest: WebTestClient
    @Autowired
    lateinit var quoteRepository: QuoteRepository

    @Test
    fun `should add quote`() {
        /* Given */

        /* When */
        val q = rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"Y a pas à dire, dès qu'il y a du dessert, le repas est tout de suite plus chaleureux !", "author":{"firstName":"Arthur", "lastName":"Pendragon"}}""")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody(Quote::class.java)
                .returnResult()
                .responseBody!!

        /* Then */
        assertThat(q.id).isGreaterThan(0)
        assertThat(q.body).isEqualTo("Y a pas à dire, dès qu'il y a du dessert, le repas est tout de suite plus chaleureux !")
        assertThat(q.author.firstName).isEqualTo("Arthur")
        assertThat(q.author.lastName).isEqualTo("Pendragon")
    }

    @Test
    fun `should add quote and fetch it`() {
        /* Given */

        val q = rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"Ah, mais des torches pareilles, on devrait les mettre sous verre, hein !", "author":{"firstName":"Arthur", "lastName":"Pendragon"}}""")
                .exchange()
                /* Then */
                .expectBody(Quote::class.java)
                .returnResult()
                .responseBody!!

        /* When */
        val secondQuote = rest.get()
                .uri("/api/v1/quotes/${q.id}")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody(Quote::class.java)
                .returnResult()
                .responseBody!!

        assertThat(secondQuote.id).isEqualTo(q.id)
        assertThat(secondQuote.body).isEqualTo(q.body)
        assertThat(secondQuote.author.firstName).isEqualTo(q.author.firstName)
        assertThat(secondQuote.author.lastName).isEqualTo(q.author.lastName)
        assertThat(secondQuote.author.id).isEqualTo(q.author.id)
    }

    @Test
    fun `should retrieve multiples quotes`() {
        /* Given */
        val q1 = rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"J’vais vous en faire préparer un de bon de sortie. Signé, tamponné, tout ce qu’il y a de plus officiel. Et puisque vous aimez les catapultes, on va vous mettre les miches dedans, vous, votre fils, votre femme, et tout l’orchestre. Et paf, feu nourri, direction Carmélide, la Bretagne vue du ciel ! Ça vous apprendra à bouffer à tous les râteliers.", "author":{"firstName":"Arthur", "lastName":"Pendragon"}}""")
                .exchange()
                /* Then */
                .expectBody(Quote::class.java)
                .returnResult()
                .responseBody!!

        val q2 = rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"Mouais. Méfiez-vous des cons. Y en a qui vont beaucoup plus loin qu'on ne pense !", "author":{"firstName":"Guenièvre", "lastName":"De Carmélide"}}""")
                .exchange()
                /* Then */
                .expectBody(Quote::class.java)
                .returnResult()
                .responseBody!!

        val q3 = rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"Donc, pour résumer, je suis souvent victime des colibris, sous-entendu des types qu’oublient toujours tout. Euh, non… Bref, tout ça pour dire, que je voudrais bien qu’on me considère en tant que Tel.", "author":{"firstName":"Perceval", "lastName":"De Galles"}}""")
                .exchange()
                /* Then */
                .expectBody(Quote::class.java)
                .returnResult()
                .responseBody!!

        /* When */
        val quotes = rest.get()
                .uri("/api/v1/quotes")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBodyList<Quote>()
                .returnResult()
                .responseBody!!

        /* Then */
        assertThat(quotes).hasSize(3)
        assertThat(quotes[0]).isEqualToComparingFieldByFieldRecursively(q1)
        assertThat(quotes[1]).isEqualToComparingFieldByFieldRecursively(q2)
        assertThat(quotes[2]).isEqualToComparingFieldByFieldRecursively(q3)
    }

    @AfterEach
    fun afterEach() {
        quoteRepository.deleteAll()
    }
}
