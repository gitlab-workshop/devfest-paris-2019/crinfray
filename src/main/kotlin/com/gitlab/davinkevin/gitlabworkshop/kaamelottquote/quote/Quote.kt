package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote

import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona.Persona
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.ManyToOne

@Entity
class Quote(
        @Id @GeneratedValue val id: Long?,
        @ManyToOne val author: Persona,
        @Column(length = 65555) val body: String
)
