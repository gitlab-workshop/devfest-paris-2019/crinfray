package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.jdbc.Sql

@DataJpaTest
@Tag("unit")
class PersonaRepositoryTest {

    @Autowired
    lateinit var repository: PersonaRepository

    @Test
    @Sql("/db/persona.findFirstByFirstNameAndLastName.sql")
    fun `should find by firstname and lastname`() {
        /* Given */
        /* When */
        val persona = repository.findFirstByFirstNameAndLastName("Guenièvre", "De Carmélide")!!
        /* Then */
        assertThat(persona.id).isNotNull()
        assertThat(persona.firstName).isEqualTo("Guenièvre")
        assertThat(persona.lastName).isEqualTo("De Carmélide")
    }

    @Test
    fun `should find no one with this name`() {
        /* Given */
        /* When */
        val persona = repository.findFirstByFirstNameAndLastName("Guenièvre", "De Carmélide")
        /* Then */
        assertThat(persona).isNull()
    }
}
