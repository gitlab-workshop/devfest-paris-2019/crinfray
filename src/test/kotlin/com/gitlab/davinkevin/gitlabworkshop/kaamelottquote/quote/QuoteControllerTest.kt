package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote

import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona.Persona
import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona.PersonaRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.ImportAutoConfiguration
import org.springframework.boot.autoconfigure.web.reactive.error.ErrorWebFluxAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.Optional

@Tag("unit")
@WebFluxTest(controllers = [QuoteController::class])
@ImportAutoConfiguration(ErrorWebFluxAutoConfiguration::class)
class QuoteControllerTest {

    @Autowired
    lateinit var rest: WebTestClient
    @MockBean
    lateinit var quoteRepository: QuoteRepository
    @MockBean
    lateinit var personaRepository: PersonaRepository

    @Test
    fun `should find all with no data`() {
        /* Given */
        whenever(quoteRepository.findAll()).thenReturn(listOf())
        /* When */
        rest.get()
                .uri("/api/v1/quotes")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody()
                .json("""[]""")
    }

    @Test
    fun `should find all with two elements`() {
        /* Given */
        val arthur = Persona(1, "Arthur", "Pendragon")
        val guenievre = Persona(2, "Guenièvre", "De Carmélide")
        val q1 = Quote(1, arthur, "Je ne pense pas que vous soyez le symbole de la nation bretonne")
        val q2 = Quote(2, guenievre, "Mais je n'ai rien à dire moi, on a rien à dire quand on est con comme une chaise !")

        whenever(quoteRepository.findAll()).thenReturn(listOf(q1, q2))

        /* When */
        rest.get()
                .uri("/api/v1/quotes")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody()
                .json("""[
                   {
                      "id":1,
                      "author":{ "id":1, "firstName":"Arthur", "lastName":"Pendragon" },
                      "body":"Je ne pense pas que vous soyez le symbole de la nation bretonne"
                   },
                   {
                      "id":2,
                      "author":{ "id":2, "firstName":"Guenièvre", "lastName":"De Carmélide" },
                      "body":"Mais je n'ai rien à dire moi, on a rien à dire quand on est con comme une chaise !"
                   }
                ]""")
    }

    @Test
    fun `should find one`() {
        /* Given */
        val arthur = Persona(1, "Arthur", "Pendragon")
        val quote = Quote(1, arthur, "De toutes façons, c'est ma mère, j'vais pas la faire tabasser par la garde.")
        whenever(quoteRepository.findById(1)).thenReturn(Optional.of(quote))
        /* When */
        rest.get()
                .uri("/api/v1/quotes/1")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody()
                .json("""{"id":1,"author":{"id":1,"firstName":"Arthur","lastName":"Pendragon"},"body":"De toutes façons, c'est ma mère, j'vais pas la faire tabasser par la garde."}""")
    }

    @Test
    fun `should create new quote with not existing persona`() {
        /* Given */
        val arthur = Persona(1, "Arthur", "Pendragon")
        val quote = Quote(1, arthur, "Ah, le printemps ! La nature se réveille, les oiseaux reviennent, on crame des mecs.")
        whenever(personaRepository.findFirstByFirstNameAndLastName(any(), any())).thenReturn(null)
        whenever(personaRepository.save(any<Persona>())).thenReturn(arthur)
        whenever(quoteRepository.save(any<Quote>())).thenReturn(quote)

        /* When */
        rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"Ah, le printemps ! La nature se réveille, les oiseaux reviennent, on crame des mecs.", "author":{"firstName":"Arthur", "lastName":"Pendragon"}}""")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody()
                .json("""{"id":1,"author":{"id":1,"firstName":"Arthur","lastName":"Pendragon"},"body":"Ah, le printemps ! La nature se réveille, les oiseaux reviennent, on crame des mecs."}""")
    }

    @Test
    fun `should create new quote with already existing persona`() {
        /* Given */
        val arthur = Persona(1, "Arthur", "Pendragon")
        val quote = Quote(1, arthur, "Merlin, i'sait déjà pas monter des blancs en neige, alors préparer une potion de polymorphie… Permettez-moi d'avoir des doutes.")
        whenever(personaRepository.findFirstByFirstNameAndLastName(any(), any())).thenReturn(arthur)
        whenever(quoteRepository.save(any<Quote>())).thenReturn(quote)

        /* When */
        rest.post()
                .uri("/api/v1/quotes")
                .contentType(MediaType.APPLICATION_JSON)
                .syncBody("""{"body":"foo", "author":{"firstName":"Arthur", "lastName":"Pendragon"}}""")
                .exchange()
                /* Then */
                .expectStatus().isOk
                .expectBody()
                .json("""{"id":1,"author":{"id":1,"firstName":"Arthur","lastName":"Pendragon"},"body":"Merlin, i'sait déjà pas monter des blancs en neige, alors préparer une potion de polymorphie… Permettez-moi d'avoir des doutes."}""")
    }
}
